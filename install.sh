#!/bin/sh

# Links scripts into local bin folder

USER_BIN_DIR="$HOME/bin"

if [ ! -d $USER_BIN_DIR ]; then
    mkdir -pv $USER_BIN_DIR
fi

# List of scripts to install
scripts=(
    audioctl 
    battery-notification
    brightctl
    configure-wacom
    reboot-required
    update-mirrorlist
    waybar-notification-module
    wlock
)

for script in ${scripts[@]}; do
    ln -sv $PWD/scripts/$script $USER_BIN_DIR/$script
    chmod u+x $USER_BIN_DIR/$script
done
