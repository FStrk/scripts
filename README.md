# Scripts

[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

This repo contains some scripts for my personal workflow.

| Name                       | Description | Requirements |
|:---------------------------|:------------|:-------------|
| audioctl                   | A wrapper for pactl which sends notifications | pactl, dunst |
| battery-notification       | Sends a notification if battery level is low | acpi, dunst |
| brightctl                  | A wrapper for controlling display brightness which sends a notification | light, dunst |
| configure-wacom            | A script to map my Wacom tablet input onto one specific monitor. <br> The script works for x11 based window managers and Sway. | X11: xrandr<br>Sway: jq
| get-workspace              | A script to get the current workspace (X11-specific) | wmctrl |
| lock                       | Custom i3lock options | i3lock |
| reboot-required            | Checks whether a reboot is required |
| update-mirrorlist          | A script to update and priorize the mirrorlist of pacman depending on bandwidth | pacman-contrib |
| waybar-notification-module | A waybar module to enable / diable notifications | dunst |
| wlock                      | Custom swaylock options | swaylock |
