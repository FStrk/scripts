#!/bin/sh

################################################################################
#                                    lock                                      #
################################################################################

# Custom i3lock options

# Requires: i3lock

################################################################################
# Copyright (C) 2022  Felix Stärk                                              #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.       #
################################################################################


themeColor=$(xrdb -query | grep "*.Colors.main_light" | cut -f2)
themeColor2=$(xrdb -query | grep "*.Colors.main_dark" | cut -f2)

B='#00000000'          # blank
C='#ffffff22'          # clear ish
D="${themeColor}cc"    # default
H="${themeColor2}bb"
T="${themeColor}ee"    # text
W='#880000bb'          # wrong
V="${themeColor}bb"    # verifying

i3lock \
--insidever-color=$C   \
--ringver-color=$V     \
\
--insidewrong-color=$C \
--ringwrong-color=$W   \
\
--inside-color=$B      \
--ring-color=$D        \
--line-color=$B        \
--separator-color=$D   \
\
--verif-color=$T        \
--wrong-color=$W        \
--time-color=$T        \
--date-color=$T        \
--layout-color=$T      \
--keyhl-color=$H       \
--bshl-color=$W        \
\
--blur 5              \
--clock               \
--indicator           \
--time-str="%H:%M:%S"  \
--date-str="%A,  %d.%m.%Y" \
\
--pass-volume-keys \
--pass-media-keys \
